---
title: A Viagem de Mustafa
layout: espectaculo
bookable: true
cover-video: "https://www.youtube-nocookie.com/embed/JhNxNAlocFc"
short-description: A história de um menino que vem de um país em guerra
credits:
  - "Concepção: Ângela Ribeiro"
  - "Figurinos: Catarina Pé-curto"
---

## Sinopse

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Urna nec tincidunt praesent semper. At lectus urna duis convallis convallis tellus. Platea dictumst vestibulum rhoncus est pellentesque elit. Amet dictum sit amet justo donec enim. Non pulvinar neque laoreet suspendisse interdum consectetur. Viverra accumsan in nisl nisi scelerisque eu ultrices vitae auctor. Nulla at volutpat diam ut. Habitant morbi tristique senectus et netus et malesuada fames. Odio eu feugiat pretium nibh ipsum consequat nisl vel pretium. Vitae semper quis lectus nulla. Vitae proin sagittis nisl rhoncus mattis rhoncus urna neque.

![Uma imagem](/assets/img/pexels-vlada-karpovich-7356566.jpg){: width="250"}

Integer feugiat scelerisque varius morbi enim nunc faucibus a. Quis viverra nibh cras pulvinar mattis nunc sed blandit libero. Enim nec dui nunc mattis enim ut. Porttitor leo a diam sollicitudin tempor id. Pellentesque pulvinar pellentesque habitant morbi. Pretium fusce id velit ut tortor pretium viverra suspendisse potenti. Tristique senectus et netus et malesuada. Convallis aenean et tortor at risus. Lorem ipsum dolor sit amet consectetur adipiscing elit. Cursus vitae congue mauris rhoncus aenean vel elit scelerisque.


## Sobre

Tortor vitae purus faucibus ornare suspendisse sed nisi. Congue nisi vitae suscipit tellus mauris a. Viverra adipiscing at in tellus integer. Id interdum velit laoreet id donec ultrices tincidunt arcu. Velit euismod in pellentesque massa placerat. Proin nibh nisl condimentum id venenatis a. Libero enim sed faucibus turpis in eu mi bibendum. Arcu dui vivamus arcu felis bibendum ut. Cursus turpis massa tincidunt dui. Pharetra vel turpis nunc eget lorem. Quis viverra nibh cras pulvinar mattis nunc sed blandit libero. Convallis convallis tellus id interdum velit laoreet id. Tellus in hac habitasse platea dictumst vestibulum rhoncus.