# Ângela Ribeiro website

This website is built with [Jekyll]. It is deployed on [netlify] and uses [forestry.io] for managing content.

It also uses [bootstrap] for the theming.

[Jekyll]: https://jekyllrb.com/
[netlify]: https://app.netlify.com/sites/sad-tereshkova-ef3f91/overview
[forestry.io]: https://app.forestry.io/sites/ial0zx8rcmpgbg/#/
[bootstrap]: https://getbootstrap.com/


## Development

Install Ruby

```
sudo apt install --yes \
    build-essential \
    ruby-full \
    zlib1g-dev
```

Configure install path for Ruby gems

```
echo '# Install Ruby Gems to ~/gems' >> ~/.bashrc
echo 'export GEM_HOME="$HOME/gems"' >> ~/.bashrc
echo 'export PATH="$HOME/gems/bin:$PATH"' >> ~/.bashrc
source ~/.bashrc
```

Install `Jekyll` and `bundler`

```
gem install jekyll bundler
```

Clone this repo

```
git clone git@gitlab.com:ricardogsilva/angela-ribeiro-website.git
```

Install the site locally

```
cd angela-ribeiro-website
bundle
```

Start the development server

```
bundle exec jekyll server --livereload
```

Now edit the code at will and check your changes on the browser